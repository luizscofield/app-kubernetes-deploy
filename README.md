# simple CI/CD pipeline to build and deploy an app on Kubernetes

This repository builds a simple docker image that runs apache httpd.
After testing and building, merging to the main branch triggers a deploy action to a kubernetes cluster.
The Kubernetes cluster and Registry sensitive variables are stored as secret environment variables.
