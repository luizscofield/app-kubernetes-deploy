FROM httpd

COPY index.html htdocs/

EXPOSE 80

CMD ["httpd-foreground"]
